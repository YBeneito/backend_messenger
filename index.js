var express = require("express")
var fs = require('fs')
var cors = require('cors')

let port = process.env.PORT || 3000


// Correspond au fichier JSON stockant les messages
let file = JSON.parse(fs.readFileSync('datas.json')) 
// Correspond au fichier JSON stockant le dernier ID utilisé
let lastId = JSON.parse(fs.readFileSync('id.json'))

var app = express()
 
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())
app.use((req,res,next) => {
    res.setHeader('Access-Control-Allow-Origin', "*")
    res.setHeader('Access-Control-Allow-Headers', "*")
    next()
})

app.get("/",(req,res)=> {
    res.send('Salut')
})


// Récupérer la liste des messages sur GET /msg
app.get("/msg",(req,res) => {
    let code = 404  
    if(file){
      code = 200
      res.status(code).send(file)
    }else{
      res.status(code).send('messages non trouvée')
    }
})

// Récupérer un messages sur GET /msg/:id
// app.get("/msg/:id",(req,res) => {
//     
// })

// Ajouter un msg sur POST /msg 
// autoIncrement de l'id et modification de id.json

app.post("/msg",(req,res) => {
    lastId.last ++;
    req.body.id = lastId.last; 
    fs.writeFileSync('id.json', JSON.stringify(lastId) )     
    req.body.read = false
    file.push(req.body)
    fs.writeFileSync('datas.json', JSON.stringify(file))
    res.setHeader('Content-Type', 'text/plain')
    res.send("ok")
})

// Update un message sur PUT /msg/:id/read pour indiquer qu'il est lu
// Si le msg est déjà lu on envois un 304 (data non modifiée)
app.put('/msg/:id/read', (req,res) => {
  let status = 0
  file.map(m => {
   if(m.id == req.params.id) {
     if(m.read == false){
      m.read = true
      status = 200
     }else{
        status = 304
     }
    }
    return m
  })
  fs.writeFileSync('datas.json', JSON.stringify(file))
  res.status(status)
  .send("Le message " + req.params.id + " est bien lu")
})


// Supprimer un message sur DELETE /msg/:id

app.delete('/msg/:id', (req,res) => {
  file.map(m => {
    if(m.id == req.params.id) {
      file.pop(m)
    }
   })
   fs.writeFileSync('datas.json', JSON.stringify(file))
  res.status(200).send('deleted')
})


app.listen(port, () => console.log("connected"));